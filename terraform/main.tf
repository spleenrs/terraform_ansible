module "yandex_cloud_network" {
  source = "./modules/tf-yc-network"
}

module "yandex_cloud_instance" {
  source = "./modules/tf-yc-instance"
  image  = var.image_id
  zone   = var.zone
  subnet = module.yandex_cloud_network.yandex_vpc_subnets.subnet_id
}

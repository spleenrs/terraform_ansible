variable "core_q" {
  description = "Number of cores"
  default     = 2
  type        = number
}

variable "memory_v" {
  description = "Volume of memory"
  default     = 4
  type        = number
}

variable "zone" {
  description = "Zone"
  type        = string
}

variable "platform" {
  description = "Platform id"
  default     = "standard-v3"
  type        = string
}

variable "image" {
  description = "Image id"
  default     = "fd80qm01ah03dkqb14lc"
  type        = string
}

variable "disk_v" {
  description = "Disk volume"
  default     = 30
  type        = number
}

variable "subnet" {
  description = "Subnet id"
  type        = string
}

resource "yandex_compute_instance" "vm" {
  name = "instance name"

  zone        = var.zone
  platform_id = var.platform
  scheduling_policy {
    preemptible = true
  }

  # Конфигурация ресурсов:
  # количество процессоров и оперативной памяти
  resources {
    cores  = var.core_q
    memory = var.memory_v
  }
  # Загрузочный диск:
  # здесь указывается образ операционной системы
  # для новой виртуальной машины
  boot_disk {
    initialize_params {
      image_id = var.image
      size     = var.disk_v
    }
  }

  # Сетевой интерфейс:
  # нужно указать идентификатор подсети, к которой будет подключена ВМ
  network_interface {
    subnet_id = var.subnet
    nat       = true
  }

  # Метаданные машины:
  # здесь можно указать скрипт, который запустится при создании ВМ
  # или список SSH-ключей для доступа на ВМ
  metadata = {
    user-data = "${file("./modules/tf-yc-instance/meta.txt")}"
  }
}

locals {
    inventory_tmpl = <<-EOT
        all:
          children:
            backend:
              hosts:
                ${yandex_compute_instance.vm.network_interface.0.ip_address}
            frontend:
              hosts:
                ${yandex_compute_instance.vm.network_interface.0.ip_address}
    EOT
}

resource "local_file" "ansible_registry" {
    filename = "/home/student/infrastructure/ansible/inventory.yaml"
    content = local.inventory_tmpl
}


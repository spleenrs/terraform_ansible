# tf-yc-network

## Terraform модуль для получения subnet_id

Данный модуль использует в качестве провайдера Yandex Cloud и возвращает subnet_id в выбранной зоне.

**Структура:**

```
tf-yc-network/
├── ReadMe.md 
├── main.tf
├── versions.tf
├── variables.tf
├── outputs.tf
```

- Файл `main.tf` содержит два datasource, с помощью которых можно получить информацию о сетях зоны. 

- Файл `versions.tf` содержит настройку провайдера  **source** и его версии **version** для Terraform.
	
- Файл `variables.tf` содерджит описание используемых в модуле переменных:
    - `network_zone` `string` зона размещения (по умолчанию ru-central1-a)

- Файл `outputs.tf` содерджит описание возвращаемых переменных:
    - `yandex_vpc_subnets` информация о подсети

# Main

## Terraform модуль для создания виртуальной машины в Yandex Cloud

Данный модуль использует в качестве провайдера Yandex Cloud и создает виртуальную машину с необходимыми параметрами, внутренним и внешним IP-адресами.


**Структура:**

```
terraform/
├── ReadMe.md 
├── main.tf
├── versions.tf
├── variables.tf
├── provider.tf
├── terraform.tfvars
├── modules/
```

- Файл `main.tf` содержит подключение двух модулей **yandex_cloud_network** и **yandex_cloud_instance**. 

- Файл `versions.tf` содержит настройку провайдера  **source** и его версии **version** для Terraform.
	
- Файл `variables.tf` содерджит описание используемых в модуле переменных:
    - `cloud_id` `string` id облака 
    - `folder_id` `string` id директории в облаке, в которой создается ВМ
    - `zone` `string` зона размещания
    - `image_id` `string` id образа OS

- Файл `provider.tf` содерджит настройку провайдера `cloud_id`, `folder_id` и `zone`.

- Файл `terraform.tfvars` содерджит инициализацию необходимых переменных `cloud_id`, `folder_id`, `zone` и `image_id`.

- Директория `modules/` содержит дочерние подключаемые модули

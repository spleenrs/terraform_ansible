variable "cloud_id" {
  type        = string
  description = "Id cloud"
}

variable "folder_id" {
  type        = string
  description = "Id folder"
}

variable "zone" {
  type        = string
  description = "Zone name"
}

variable "image_id" {
  type        = string
  description = "Id image"
}


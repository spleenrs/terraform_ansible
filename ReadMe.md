## Infrastructure as Code

В данном примере с помощью модулей Terraform создается виртуальная машина (ВМ) в Yandex Cloud, полученный IP-адрес сохраняется в файл inventory.yaml для использования в Ansible. 
На созданной ВМ, используя инструменты Ansible, можно развернуть приложение (Java/Node.js).

На ВМ будут установлены:

- `openjdk-16-jdk`
- `nodejs`
- `nginx`
- `systemd` юнит для backend


## Команды:

**Terraform**
```
terraform init
terraform plan
terraform apply
```

**Ansible**
```
ansible-playbook playbook.yaml --ask-vault-pass
```

## Доступ к сервису: 
`http://IP_ADDRESS_VM/`
